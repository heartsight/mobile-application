import requests
import base64
import zlib
import json
from sys import stdout
import os

# -------------
# Hidden config file
server_url = str(json.load(open("config.json"))["server_url"])

def verify_user(user_id, household_id='', debugging=False) -> bool:
    try:
        r = requests.post('https://pulse.heartvision.cloud/Household/verifyUser', json={
            "userId":user_id,
            "householdId":household_id
        })
        
        return r.json()

    except Exception as ex:
        if debugging: stdout.write(f"\rException occurred trying to handle verify_user: {ex}\n")
        return False

def verify_household(household_id, debugging=False) -> bool:
    try:
        r = requests.post('https://pulse.heartvision.cloud/Household/verifyHousehold', json={
            "householdId":household_id
        })

        return r.json()
    
    except Exception as ex:
        if debugging: stdout.write(f"\rException occurred trying to handle verify_household: {ex}\n")
        return False

def post_raw_video_to_server(user_id, household_id, video, fps=30, device_id="dummy_device", device_type="dummy_type", debugging=False):
    if debugging: print(user_id, household_id, video.shape, fps, device_id, device_type)

    try:
        r = requests.post(f'{server_url}/analyze', data={
            "user_id": user_id,
            "household_id": household_id,
            "video": zlib.compress(base64.b64encode(video)).decode("utf-8"),
            "video_shape": zlib.compress(base64.b64encode(video.shape)).decode("utf-8"),
            "fps": fps,
            "device_id": device_id,
            "device_type": device_type
        })

        if debugging: stdout.write(f"\rResponse {r.status_code}")
    except Exception as ex:
        if debugging: stdout.write(f"\rException occurred trying to post_video_to_server: {ex}\n")
        return False

def post_video_to_server(user_id, household_id='', video_filename='', device_id="dummy_device", device_type="dummy_type", debugging=False):
    file = open(video_filename, "rb")

    try:
        data = {
            "user_id": str(user_id),
            "household_id": str(household_id),
            "device_id": device_id,
            "device_type": device_type
        }

        r = requests.post(f'{server_url}/analyze', 
            data=data, 
            files={"video":file},
            headers={}
        )
        
        file.close()
        os.remove(video_filename)

        if debugging: stdout.write(f"\rResponse {r.status_code}" + ' ' * 60 + "\n")
        return r.status_code == 200
    except Exception as ex:
        if debugging: stdout.write(f"\rException occurred trying to post_raw_video_to_server: {ex}\n")
        return False
    
