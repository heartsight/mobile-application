import cv2
import numpy as np
import time
import scripts.api as api
from threading import Thread
from sys import stdout

# ---------------
# Hyperparameters
# ---------------

# Duration in seconds for bad qr codes to be removed from the cache.
qr_refresh_duration = 300
# Padding in % of area around center
video_padding = 80
# Debugging options
debug_mode = True

_scanned_data_cache = ['']
current_household_id = None
face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
qcd = cv2.QRCodeDetector()

def start_video_process(duration, fps=30):
    global _scanned_data_cache
    global current_household_id

    max_length = int(duration * fps)

    threaded_cam = ThreadedCamera(bufferSize=max_length, fps=fps)

    start_time = time.time()

    while not threaded_cam.thread.is_alive(): 
        time.sleep(1)
        stdout.write(f"\rWaiting for thread to start..." + ' ' * 20)
    time.sleep(1)

    while True:
        frame = threaded_cam.frame

        # If the refresh time has passed, reset the data cache.
        if time.time() >= qr_refresh_duration + start_time:
            _scanned_data_cache = ['']
            start_time = time.time()

        for code, center in iterate_codes_in_img(frame):
            if code not in _scanned_data_cache and code != current_household_id:
                stdout.write(f"\rFound new code: {code}" + ' ' * 20 + "\n")

                Thread(target=process_code, args=(
                    code, 
                    center, 
                    threaded_cam.get_buffer(),
                    fps
                    )
                ).start()
                _scanned_data_cache.append(code)

        if debug_mode: threaded_cam.show_frame()

# credit nathancy on StackOverflow
class ThreadedCamera(object):
    def __init__(self, src=0, bufferSize = 90, fps=30):
        self.capture = cv2.VideoCapture(src)
        self.capture.set(cv2.CAP_PROP_BUFFERSIZE, 2)
       
        # FPS = 1/X
        # X = desired FPS
        self.FPS = 1/fps
        self.FPS_MS = int(self.FPS * 1000)

        self.currentIdx = 0
        self.frameBuffer = [None] * bufferSize
        
        # Start frame retrieval thread
        self.thread = Thread(target=self.update, args=())
        self.thread.daemon = True
        self.thread.start()
        
    def update(self):
        while True:
            if self.capture.isOpened():
                (self.status, self.frame) = self.capture.read()
                self.frameBuffer[self.currentIdx] = self.frame

                self.currentIdx += 1
                if self.currentIdx >= len(self.frameBuffer):
                    self.currentIdx -= len(self.frameBuffer)

            time.sleep(self.FPS)
            
    def show_frame(self):
        cv2.imshow('frame', self.frame)
        cv2.waitKey(self.FPS_MS)

    def get_buffer(self):
        return self.frameBuffer[self.currentIdx:] + self.frameBuffer[:self.currentIdx]

# For processing a QR code
def process_code(code, center, frames, fps):
    global _scanned_data_cache
    global current_household_id

    if api.verify_household(code):
        if debug_mode: stdout.write(f"\rFound that {code} is a household id!" + ' ' * 20 + "\n")
        current_household_id = code

        # the cache might contain valid user_id that we couldnt scan before
        _scanned_data_cache = ['']

        # nothing more to do if this is a household id
        return

    if api.verify_user(code, current_household_id):
        if debug_mode: stdout.write(f"\rFound that {code} is a user id!" + ' ' * 20 + "\n")
        process_video_for_server(code, center, frames, fps)
        return
    
    if debug_mode: stdout.write(f"\rFound that {code} is not a valid QR code." + ' ' * 20 + "\n")

# Processing video to be sent to the server for a user
def process_video_for_server(user_id, center, frames, fps=30):
    global current_household_id

    # face = find_closest_face_to_qr(center, frames[0])

    # cropping broken rn
    # video = crop_video(np.asarray(frames), face[0], face[1], face[2], face[3])
    
    output_file = 'video.mp4'
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    output_video = cv2.VideoWriter(output_file, fourcc, fps, (frames[0].shape[1], frames[0].shape[0]))

    for i in range(len(frames)):
        output_video.write(frames[i])

    output_video.release()

    api.post_video_to_server(user_id, current_household_id, output_file, debugging=debug_mode)

# Finds the closest face to the given coordinates
# If no faces are given, it returns the entire image.
def find_closest_face_to_qr(center, img):
    faces = face_cascade.detectMultiScale(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY), 
                                          scaleFactor=1.1, minNeighbors=5, minSize=(30, 30))
    
    if len(faces) == 0:
        if debug_mode: stdout.write("\rError: No faces found!" + ' ' * 20 + "\n")
        return (0, 0, img.shape[1], img.shape[0])
    
    face = faces[0]
    for (x,y,w,h) in faces:
        dist1 = (x + w/2 - center[0]) ** 2 + (y + h/2 - center[1]) ** 2
        dist2 = (face[0] + face[2]/2 - center[0]) ** 2 + (face[1] + face[3]/2 - center[1]) ** 2

        if dist1 < dist2:
            face = (x,y,w,h)

    return face

# Iterates through all QR codes in an image.
def iterate_codes_in_img(img):
    global qcd

    retval, decoded_info, points, straight_qrcode = qcd.detectAndDecodeMulti(img)

    for i in range(len(decoded_info)):
        yield (
            decoded_info[i],
            np.mean(points[i],axis=1)
        )

# Crops a numpy video with the given parameters
def crop_video(video, x, y, w, h):
    x, y = x + w/2, y + h/2
    scalar = (1 + video_padding / 100)
    w, h = w * scalar, h * scalar

    ix1, ix2 = int(max(x-w/2, 0)), int(min(x+w/2, video.shape[1]))
    iy1, iy2 = int(max(y-h/2, 0)), int(min(y+h/2, video.shape[2]))

    return video[:, ix1:ix2, iy1:iy2, :]