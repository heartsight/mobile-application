import cv2
import numpy as np
import ffmpeg
import requests

# Replace these values with your desired video parameters
width = 640
height = 480
fps = 30
duration_in_seconds = 5

# Function to generate the raw video frames (replace this with your capture logic)
def generate_raw_frames():
    for _ in range(fps * duration_in_seconds):
        frame = np.random.randint(0, 256, (height, width, 3), dtype=np.uint8)
        yield frame

# Create a VideoCapture object to capture frames in memory
video_capture = cv2.VideoCapture("video.mp4", apiPreference=cv2.CAP_IMAGES)

# Define the codec and create an FFmpeg writer
output_filename = "output.mp4"
output_codec = "libx264"  # For H.264 video codec, which is widely supported
output_bitrate = "1000k"  # Adjust the bitrate as needed

ffmpeg_writer = (
    ffmpeg.input("pipe:0", format="rawvideo", pix_fmt="rgb24", s="{}x{}".format(width, height))
    .output(output_filename, vcodec=output_codec, r=fps, b=output_bitrate)
    .overwrite_output()
    .run_async(pipe_stdin=True)
)

# Store the video data in a bytes buffer to avoid saving locally
video_data = bytes()

# Read frames from the VideoCapture object, process (if needed), and store the video data
while True:
    ret, frame = video_capture.read()
    if not ret:
        break

    # Add your processing logic here if required (e.g., resizing, filtering, etc.)

    # Write the frame to the FFmpeg writer
    ffmpeg_writer.stdin.write(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB).tobytes())

    # Append the frame data to the video_data buffer
    video_data += cv2.imencode('.jpg', frame)[1].tobytes()

# Close the FFmpeg writer
ffmpeg_writer.stdin.close()
ffmpeg_writer.wait()

# Release the VideoCapture object
video_capture.release()

# Define the server URL where you want to send the video
server_url = "https://your-server-url/upload"

# Set the desired headers for the POST request (optional)
headers = {
    "Content-Type": "video/mp4",  # Adjust the content type according to your server's requirements
}

# Send the video data to the server using a POST request
response = requests.post(server_url, data=video_data, headers=headers)

# Check the response from the server (optional)
if response.status_code == 200:
    print("Video sent successfully.")
else:
    print("Failed to send the video.")
