import scripts.img_handler as img_handler

# Duration in seconds of the video recorded every refresh
video_duration = 5

if __name__ == "__main__":
    print("Launching application!")
    img_handler.start_video_process(video_duration, fps=30)